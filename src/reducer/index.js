import { combineReducers } from 'redux';
import actions from '../actions';

const _reducers = {};
for (let modName in actions) {
  const { reducers, store } = actions[modName] || {};
  if (reducers) {
    _reducers[modName] = (state = store, action) => {
      const { type, payload } = action;
      const { [type]: reducer } = reducers;
      if (reducer) {
        const _state = { ...state, __time: new Date().getTime() + Math.random() };
        return { ...state, ...reducer(_state, payload) };
      } else {
        return state;
      }
    };
  }
}

export default combineReducers(_reducers);
