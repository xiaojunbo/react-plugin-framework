// 默认数据
export default {
  themes: [
    {
      name: 'light'
    },
    {
      name: 'dark'
    }
  ],
  activeTheme: 'light'
};

// 设置主题
export function SET_THEME(state, payload) {
  state.activeTheme = payload;
  document.documentElement.setAttribute('theme', payload);
  return state;
}
