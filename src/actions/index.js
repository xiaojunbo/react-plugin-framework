import ars from './ars';
import * as general from './general';
import * as sidebar from './sidebar';

const actions = {
  general,
  sidebar
};

export default ars(actions);
