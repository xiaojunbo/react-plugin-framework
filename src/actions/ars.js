const ars = (actions) => {
  let ars = {};
  for (let modName in actions) {
    const mod = actions[modName];
    ars[modName] = { store: {}, reducers: {}, sages: {}, other: {} };
    for (let key in mod) {
      const val = mod[key];
      if (key === 'default') {
        ars[modName]['store'] = val;
      } else {
        const { constructor: { name: funType } = {} } = val;
        if (funType) {
          if (funType === 'GeneratorFunction') {
            ars[modName]['sages'][key] = val;
          } else {
            ars[modName]['reducers'][key] = val;
          }
        } else {
          ars[modName]['other'][key] = val;
        }
      }
    }

    // 添加一个通用reducer
    if (!ars[modName]['reducers']['$']) {
      ars[modName]['reducers']['$'] = function (state, { key = '', val }) {
        const keys = key.split('.');
        const len = keys.length;
        let _state = state;
        keys.forEach((key, idx) => {
          _state[key] = _state[key] || {};
          if (idx === len - 1) {
            _state[key] = val;
          }
        });
        return state;
      };
    }
  }
  return ars;
};

export default ars;
