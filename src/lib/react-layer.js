import React from 'react';
// 动画事件
var amend = (function () {
  var evs = {
    WebkitAnimation: 'webkitAnimationEnd',
    animation: 'animationend'
  };
  for (var k in evs) {
    if (typeof evs[k] === 'string') {
      return evs[k];
    }
  }
})();
var fun = () => {};

// 任务队列
const _layerList = {};

// 缓存队列
const _cacheList = {};

// 密码列表
const _PassCode = {};

// 通过ID获取图层
const LayerById = (id) => {
  return _layerList[id];
};

const LayerByKey = (task) => {
  const [id, key] = task.split('.');
  return LayerById(id).state.tasks[key];
};

// 弹层根容器
class LayerView extends React.Component {
  constructor(props) {
    super(props);
    const { id } = this.props;
    // 判断弹层ID是否重复，如果重复则后面的覆盖前面的并打印警告
    if (_layerList[id]) {
      console.warn('ERROR: LayerView的Id重复注册', id);
    } else {
      // 注册弹层ID
    }
    _layerList[id] = this;

    this.state = {
      content: null,
      tasks: Object.assign({}, _cacheList[id]),
      cache: {},
      queue: []
    };

    // 清理缓存
    _cacheList[id] = {};
  }

  /**
   * key                            String               *     弹框维一标识符
   * passCode                       String                     密码
   * content                        React.component      *     内容
   * dark                           Number                     暗层 0-1
   * pos                            String                     弹层位置【左上-lt，中上-ct，右上-rt，左中-lm，中中-cm，右中-rm，左下-lb，中下-cb，右下-rb】 默为cm
   * autoHide                       Boolen                     自动隐藏
   * darkClickClose                 Boolen                     是否点击暗层关闭图层
   * mode                           String                     模式【覆盖-cover，缓存-cache，排队-queue，叠加-stack】
   * time                           Number                     动画时间
   * animate                        Object                     动画配置
   * on                             Object                     回调事件集合
   * on.show                        Function                   显示时执行与cb一样。
   * on.hide                        Function                   隐藏时执行
   */
  show(param = {}, cb = () => {}) {
    let {
      key,
      pos = 'cm',
      autoHide = 0,
      content = '',
      mode = 'stack',
      dark = 0.8,
      passCode = '',
      darkClickClose = 1,
      time = 350,
      cache = 0,
      animate = 'ab',
      on: { show = fun, hide = fun, dark: onDark = fun } = {}
    } = param;
    // 组件内的静态回调函数
    const { type: { on: { show: innerOnShow = fun, hide: innerOnHide = fun, dark: innerOnDark = fun } = {} } = {} } = content;

    // 合并参数与设置默认值
    let task = {
      key,
      pos,
      autoHide,
      content,
      mode,
      dark,
      status: 2,
      passCode,
      darkClickClose,
      time,
      cache,
      animate,
      ref: React.createRef(),
      on: {
        show: [show, innerOnShow],
        hide: [hide, innerOnHide],
        dark: [onDark, innerOnDark]
      }
    };

    // 显示这例弹层实例
    let _show = () => {
      // 储存私钥
      _PassCode[task.key] = task.passCode;
      // 从暴露到外面的state属性中删除私钥
      delete task.passCode;
      this.anim(task, 2, () => {
        cb(task);
        if (task.autoHide) {
          task._autoHideTimer = setTimeout(() => {
            this.anim(task, 1, cb);
          }, task.autoHide);
        }
      });
    };

    // 判断入参中是否有必填项key和content
    if (task.key && task.content) {
      // 判断这个弹层是否本身就已显示
      if (this.state.tasks[task.key]) {
        // 实例已经存在队列中
        if (_PassCode[task.key] === task.passCode) {
          // 是同一个，把这个弹层提取最前面
          this.hide({ key: task.key, passCode: task.passCode }, () => {
            this.show(task, cb);
          });
        } else {
          // 非同一个
          console.warn('KEY值已被占用，请修改后重试！', task.key);
        }
      } else {
        // 实例没有存在队列中
        if (mode === 'cover') {
          // 覆盖模式-cover
          let _tasks = this.state.tasks;
          for (let key in _tasks) {
            this.hide({ key });
          }
          _show();
        } else if (mode === 'cache') {
          // 缓存模式-cache
        } else if (mode === 'queue') {
          // 排队模式-queue
          let _queue = this.state.queue;
          _queue.push(task);
          this.setState({ queue: _queue });
        } else {
          // 默认为叠加模式-stack
          _show();
        }
      }
    } else {
      console.warn('缺少必填入参KEY或CONTENT');
    }
  }

  anim(task, status, cb = () => {}) {
    task.status = status;
    // 将这例弹层实例加入到任务队列中
    let _tasks = this.state.tasks;
    if (status) {
      _tasks[task.key] = task;
    } else {
      delete _tasks[task.key];
    }
    task.__anim_timer = null;
    let __anim = () => {
      clearTimeout(task.__anim_timer);
      clearTimeout(task._autoHideTimer);
      if (status) {
        if (task.status === 1) {
          this.anim(task, 0);
          task.on.hide.forEach((fun) => {
            fun(task);
          });
        }
        if (status === 2) {
          task.on.show.forEach((fun) => {
            fun(task);
          });
          this.anim(task, 3);
        }
        if (task.ref.current) {
          task.ref.current.removeEventListener(amend, __anim, false);
        }
        cb(task, status);
      }
    };

    this.setState({ tasks: _tasks }, () => {
      if (status) {
        task.ref.current.addEventListener(amend, __anim, false);
        task.ref.current.addEventListener('touchmove', function (e) {
          e.preventDefault();
          const stop = this.querySelectorAll('[stop="touchmove"]');
          for (let i = 0, len = stop.length; i < len; i++) {
            stop[i].addEventListener('touchmove', (e) => {
              e.stopPropagation();
            });
          }
        });
        if (status === 1) {
          // 隐藏后
          task.__anim_timer = setTimeout(() => {
            this.anim(task, 0);
            clearTimeout(task.__anim_timer);
            cb(task, 0);
          }, 1000);
        }
      }
    });
  }

  hide(param = {}, cb = () => {}) {
    const { key, passCode = '', on: { show = fun, hide = fun } = {} } = param;
    if (key) {
      let task = this.state.tasks[key];
      if (task) {
        task.on.show.push(show);
        task.on.hide.push(hide);
        if (!_PassCode[key] || _PassCode[key] === passCode) {
          this.anim(task, 1, cb);
        } else {
          console.warn('私钥验证失败无权关闭弹层');
        }
      } else {
        console.warn('当前没有KEY为"' + key + '"的弹层');
      }
    }
  }

  componentDidUpdate() {
    // 显示这例弹层实例
    let _show = (task, cb = fun) => {
      // 储存私钥
      _PassCode[task.key] = task.passCode;
      // 从暴露到外面的state属性中删除私钥
      delete task.passCode;
      this.anim(task, 2, () => {
        cb(task);
        if (task.autoHide) {
          setTimeout(() => {
            this.anim(task, 1, cb);
          }, task.autoHide);
        }
      });
    };
    let _tasks = this.state.tasks;
    let _queue = this.state.queue;
    if (!Object.keys(_tasks).length && _queue.length) {
      // console.warn('--->', _tasks, _queue);
      _show(_queue[0], () => {
        _queue.shift();
        this.setState({ queue: _queue });
      });
    }
  }

  componentDidMount() {
    let _tasks = this.state.tasks;
    for (let key in _tasks) {
      if (_tasks[key].ref.current) {
        _tasks[key].ref.current.addEventListener('touchmove', function (e) {
          e.preventDefault();
          const stop = this.querySelectorAll('[stop="touchmove"]');
          for (let i = 0, len = stop.length; i < len; i++) {
            stop[i].addEventListener('touchmove', (e) => {
              e.stopPropagation();
            });
          }
        });
      }
    }
  }

  // 根组件被卸载时销毁实例
  componentWillUnmount() {
    const { id } = this.props;
    // 缓存实例
    let _cache = {};
    let _tasks = _layerList[id].state.tasks;
    for (let key in _tasks) {
      if (_tasks[key].cache) {
        _cache[key] = _tasks[key];
      }
    }
    _cacheList[id] = _cache;
    delete _layerList[id];
  }

  render() {
    const {
      state: { tasks = {} },
      props: { style = {} }
    } = this;
    let _style = Object.assign({}, style);
    return Object.keys(tasks).map((key) => {
      let task = tasks[key];
      _style.backgroundColor = `rgba(0,0,0,${task.dark})`;
      return task.status ? (
        <div
          ref={task.ref}
          className="fekit-layer"
          key={task.key}
          data-key={task.key}
          style={_style}
          view={task.status}
          am-view={task.animate}
          onClick={(ev) => {
            if (ev.target === task.ref.current && task.status === 3) {
              task.darkClickClose &&
                this.hide({ key: task.key, passCode: _PassCode[key] }, () => {
                  task.on.dark.forEach((fun) => {
                    fun({ task, from: 'dark' });
                  });
                });
            }
          }}>
          <div className="fekit-layer-warp" data-pos={task.pos}>
            {task.content}
          </div>
        </div>
      ) : null;
    });
  }
}

export { LayerView, LayerById, LayerByKey };
