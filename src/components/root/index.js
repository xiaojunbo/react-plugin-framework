// 样式
import './root.scss';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import action from '../../store';
import { LayerById, LayerView } from '../../lib/react-layer';
import Aaa from '../aaa';
import imgLogo from './img/logo.png';

@connect((state) => state, (dispatch) => bindActionCreators(action, dispatch))
class Root extends Component {
  render() {
    console.log(48);
    return (
      <React.Fragment>
        <div className="middle">
          <img src={imgLogo} alt="" />
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'aa' });
            }}>
            示例1 位置:cm 动画:aa
          </div>
          <div
            onClick={() => {
              LayerById('root').show({
                key: 'aaa',
                content: <Aaa />,
                pos: 'cm',
                animate: 'ab',
                on: {
                  show: () => {
                    console.log('show show onclick');
                  },
                  hide: () => {
                    console.log('show hide onclick');
                  }
                }
              });
            }}>
            示例2 位置:cm 动画:ab
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'ac' });
            }}>
            示例2 位置:cm 动画:ac
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'ad' });
            }}>
            示例2 位置:cm 动画:ad
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'ae' });
            }}>
            示例2 位置:cm 动画:ae
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'af' });
            }}>
            示例2 位置:cm 动画:af
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'ag' });
            }}>
            示例2 位置:cm 动画:ag
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'ah' });
            }}>
            示例2 位置:cm 动画:ah
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'ai' });
            }}>
            示例2 位置:cm 动画:ai
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'aj' });
            }}>
            示例2 位置:cm 动画:aj
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'ak' });
            }}>
            示例2 位置:cm 动画:ak
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'rm', animate: 'al' });
            }}>
            示例2 位置:rm 动画:al
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cb', animate: 'am' });
            }}>
            示例2 位置:cb 动画:am
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'an' });
            }}>
            示例2 位置:cm 动画:an
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'ao' });
            }}>
            示例2 位置:cm 动画:ao
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cb', animate: 'ap' });
            }}>
            示例2 位置:cb 动画:ap
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'aq' });
            }}>
            示例2 位置:cm 动画:aq
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'ar' });
            }}>
            示例2 位置:cm 动画:ar
          </div>
          <div
            onClick={() => {
              LayerById('root').show({ key: 'aaa', content: <Aaa />, pos: 'cm', animate: 'as' });
            }}>
            示例2 位置:cm 动画:as
          </div>
        </div>
        <LayerView id="root" />
      </React.Fragment>
    );
  }
}

export default Root;
