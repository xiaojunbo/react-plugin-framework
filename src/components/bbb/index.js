// 样式
import React, { Component } from 'react';
import { LayerById } from '../../lib/react-layer';
import Aaa from '../aaa';

class Bbb extends Component {
  render() {
    return (
      <div className="layer">
        <div
          onClick={() => {
            LayerById('root').show({ key: 'ccc', content: <Aaa />, animate: 'ad', mode: 'queue' }, (task) => {
              console.log(task);
            });
            LayerById('root').show({ key: 'ddd', content: <Aaa />, animate: 'ad', mode: 'queue' }, (task) => {
              console.log(task);
            });
            LayerById('root').show({ key: 'eee', content: <Aaa />, animate: 'ad', mode: 'queue' }, (task) => {
              console.log(task);
            });
            LayerById('root').show({ key: 'fff', content: <Aaa />, animate: 'ad', mode: 'cover' }, (task) => {
              console.log(task);
            });
            LayerById('root').show({ key: 'ggg', content: <Aaa />, animate: 'ad', mode: 'stack' }, (task) => {
              console.log(task);
            });
            LayerById('root').show({ key: 'hhh', content: <Aaa />, animate: 'ad', mode: 'queue' }, (task) => {
              console.log(task);
            });
          }}
        >
          打开弹层2
        </div>
        <br />
        <br />
        <div>BBB</div>
      </div>
    );
  }
}

export default Bbb;
