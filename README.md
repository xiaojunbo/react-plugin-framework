# react-plugin-frameworks

REACT插件开发框架，基于官方 CREATE-REACT-APP 非 EJECT 式配置。可以无缝升级 REACT 及其插件！

## package.json

```
{
  "name": "react-framework",
  "version": "0.1.0",
  "private": true,
  "scripts": {
    "start"                                       开发环境打包
    "build"                                       测试环境打包
    "babel"                                       上线环境打包
    "npmjs"                                       打包NPM插件的命令
  },
  "eslintConfig": {                               Eslint 代码码检测
    "root": true,
    "env": {
      "node": true
    },
    "extends": "react-app",                       指定使用react框架的代码语法和风格
    "rules": {
      "indent": [
        "error",
        2,                                        缩进2个字符
        {
          "SwitchCase": 1,                        在switch遍历中缩进一格以便跟eslint默认检测语法匹配不致报错
          "flatTernaryExpressions": true,         jsx中三元运算下和map中代码缩进与eslint语法匹配不致报错
          "ignoredNodes": [
            "JSXElement *"
          ]
        }
      ],
      "quotes": ["error","single"],               使用单引号
      "semi": ["error","always"],                 行尾要分号
      "no-alert": "off",                          关闭 alert 警告
      "no-console": "off"                         关闭 console 警告
      "import/no-anonymous-default-export": [     允许直接导出未经变量的对象 export default {}
        2,
        {
          "allowObject": true
        }
      ]
    },
    "parserOptions": {
      "parser": "babel-eslint"                    让 Eslint 支持一些未来的 js 语法，比如修饰符 @
    }
  },
  "browserslist": [],                             设置兼容到浏览多低的版本?
  "devDependencies": {
    "@babel/core"                                 ES6 转 ES5 所需的插件
    "@babel/plugin-proposal-decorators"           JS 修饰符 @ 支持插件
    "@babel/polyfill"                             ES6 转 ES5 所需的插件
    "@babel/preset-env"                           ES6 转 ES5 所需的插件
    "@babel/preset-react"                         React 框架 ES6 转 ES5 所需的插件
    "babel-eslint"                                让 eslint 识别 修饰符 @
    "core-js"                                     干嘛的不知道，只知道是webpack 打包必要的基础插件
    "customize-cra"                               "react-app-rewired" 2.1.x后新版本支持插件
    "eslint"
    "eslint-plugin-react"
    "filemanager-webpack-plugin"                  将文件打包成压缩文件
    "gulp"                                        GULP 打包
    "gulp-autoprefixer"                           GULP 打包时给 CSS 加兼容性前缀
    "gulp-clean"                                  GULP 打包时删除文件或目录
    "gulp-clean-css"                              GULP 压缩 CSS 代码
    "gulp-group-css-media-queries"                这个可以把 CSS 中的媒体查询等合并在一起进一步合并压缩 CSS， 超好用。
    "gulp-rename"                                 GULP 包括时重命名文件
    "gulp-sass"                                   GULP 打包 SCSS 预编译文件
    "node-sass"                                   SCSS 预编译所需的插件
    "react-app-rewired"                           自定义 webpack 配置 config-overrides.js 替换系统配置的插件
    "rollup"                                      Rollup 打包
    "rollup-plugin-babel"                         rollup 的 babel 插件，ES6转ES5
    "rollup-plugin-commonjs"                      将非ES6语法的包转为ES6可用
    "rollup-plugin-node-resolve"                  帮助寻找node_modules里的包
    "rollup-plugin-replace"                       替换待打包文件里的一些变量，如 process在浏览器端是不存在的，需要被替换
    "rollup-plugin-terser"                        压缩JS代码的插件
    "tinypng-webpack-plugin"                      图片压缩插件
  },
  "dependencies": {
    "@mcui/core":                                 MCUI框架非必须
    "react":                                      React
    "react-dom":                                  React必要的基础插件
    "react-scripts":                              React必要的基础插件
  },
  "postcss": {
    "plugins": {
      "autoprefixer":                             给 css 加兼容性前缀
    }
  },
  "tinypngKey":                                   这个是图片无损压缩 Tinypng 的序列号
}

```
