let gulp = require('gulp');
// 清理文件和目录
let clean = require('gulp-clean');
// 处理SCSS
let sass = require('gulp-sass');
// 自动加前缀
let autoprefixer = require('gulp-autoprefixer');
// 压缩代码
let mincss = require('gulp-clean-css');
// 合并媒体查询(可以减少很多代码)
let gcmq = require('gulp-group-css-media-queries');
var rename = require('gulp-rename');

// 清理目录
let cleanup = () => {
  return gulp.src(['./npm/**/*.css']).pipe(clean());
};

let style = () => {
  return gulp
    .src(['./src/lib/**/*.scss'])
    .pipe(sass())
    .pipe(
      autoprefixer({
        overrideBrowserslist: ['> 1%', 'last 2 versions', 'not ie <= 10', 'ios >= 8', 'android >= 4.0'],
        cascade: false
      })
    )
    .pipe(gcmq())
    .pipe(mincss())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./npm'));
};
let pro = gulp.series(cleanup, style);

gulp.task('default', pro);
